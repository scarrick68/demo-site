#!/bin/bash          

git add -u
read commitMsg
git commit -m "$commitMsg"
echo "Would you like to pull from the remote repo? (y/n)"
read pull
if [$pull = "y"]; then
	git pull origin master
fi
read push
if [$push = "y"]; then
	git push origin master
fi