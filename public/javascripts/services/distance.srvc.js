(function(){
    'use strict';

    angular
        .module('app')
        .factory('distanceSrvc', distanceSrvc);

    distanceSrvc.$inject = [];


    function distanceSrvc(){
        var service = {
            kmToNauticalMiles: kmToNauticalMiles,
            haversine: haversine,
            midPoint: midPoint,
            milesToKm: milesToKm
        };

        return service;

        // convert km to nautical miles
        function kmToNauticalMiles(km) {
            return km * .539957
        }

        // calculate haversine distance in km or miles.
        function haversine(start, end, options) {
            var km = 6371;
            var mile = 3960;
            options = options || {};

            var R = options.unit === 'mile' ?
                mile :
                km;

            var dLat = toRad(end.latitude - start.latitude);
            var dLon = toRad(end.longitude - start.longitude);
            var lat1 = toRad(start.latitude);
            var lat2 = toRad(end.latitude);

            var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

            if (options.threshold) {
                return options.threshold > (R * c)
            } else {
                return R * c
            }
        }

        // calculate midPoint between the two {longitude, latitude} objects
        function midPoint(origin, destination) {
            var dLon = toRad(destination.longitude - origin.longitude);

            var lat1 = toRad(origin.latitude);
            var lon1 = toRad(origin.longitude);
            var lat2 = toRad(destination.latitude);

            var Bx = Math.cos(lat2) * Math.cos(dLon);
            var By = Math.cos(lat2) * Math.sin(dLon);

            var y = Math.sin(lat1) + Math.sin(lat2);
            var x = Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By);

            var midLat = Math.atan2(y, x);
            var midLng = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);

            var mid = {
                latitude: toDegrees(midLat),
                longitude: toDegrees(midLng)
            };

            return mid
        }

        function milesToKm (miles){
            return miles * 1.60934
        }

        // degrees to radians
        function toRad(num){
            return num * Math.PI / 180
        }

        // radians to degrees
        function toDegrees(num){
            return num * 180 / Math.PI
        }
    }
})();
