(function() {
    'use strict';

    angular
        .module('app')
        .factory('contactSrvc', contactSrvc);

    contactSrvc.$inject = ['$http', '$q'];

    function contactSrvc($http, $q){
        var service = {
            sendEmail: sendEmail
        };

        return service;

        // email was sent or it wasn't. no need to pass response back.
        function sendEmail(msg){
            return $http.post('http://54.86.9.16/api/contact-me', msg).then(
                function (response) {
                    if(response.data.error){
                        return $q.reject()
                    }
                    return
                },
                function (response) {
                    return
                }
            )
        }
    }
})();