(function() {
    'use strict';

    angular
        .module('app')
        .factory('airportSrvc', airportSrvc);

    airportSrvc.$inject = ['$http', '$q'];

    function airportSrvc($http, $q){
        var service = {
            getAirports: getAirports
        };

        return service;

        // get a list of airport data objects for autocomplete textbox
        function getAirports(){
            return $http.get('http://54.86.9.16/api/get-airport-list').then(
                function(response){
                    if(response.data.error){
                        return $q.reject(response.data.error)
                    }
                    return response.data
                },
                // nothing to return here because it doesn't affect the err I will show users.
                function(){
                    return
                }
            );
        }
    }
})();