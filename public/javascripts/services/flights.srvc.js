(function() {
    'use strict';

    angular
        .module('app')
        .factory('flightSrvc', flightSrvc);

    flightSrvc.$inject = ['$http', '$q'];

    function flightSrvc($http, $q){
        var service = {
            getFlights: getFlights
        };

        return service;

        // get flight prices and information from google flights API
        // getting more info from server than I'm using on client because I'll probably use it later.
        function getFlights(tripDetails){
            return $http.post('http://54.86.9.16/api/get-flight-info', {tripDetails: tripDetails}).then(
                function (response) {
                    if(response.data.error){
                        return $q.reject(response.data)
                    }
                    return response.data
                },
                function (response) {
                    return response.data
                });
        }
    }
})();