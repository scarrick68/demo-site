var app = angular.module('app', [
                                'ngRoute',
                                'ngAnimate',
                                'mgcrea.ngStrap',
                                'uiGmapgoogle-maps',
                                'angulartics',
                                'angulartics.google.analytics']);

app.config(['$routeProvider', '$locationProvider', 'uiGmapGoogleMapApiProvider', '$typeaheadProvider',
            function($routeProvider, $locationProvider, uiGmapGoogleMapApiProvider, $typeaheadProvider){
                $locationProvider.html5Mode(true);
                $routeProvider
                    .when('/', {templateUrl: 'partials/index.jade'})
                    .when('/airports', {templateUrl: 'partials/airports.jade', controller: 'mapCtrl'})
                    .when('/flight-info', {templateUrl: 'partials/flight-info.jade', controller: 'flightCtrl'})
                    .when('/about', {templateUrl: 'partials/about.jade'})
                    .when('/projects', {templateUrl: 'partials/projects.jade'})
                    .when('/contact', {templateUrl: 'partials/contact.jade', controller: 'contactCtrl'})
                    .otherwise({redirectTo: '/'});
                uiGmapGoogleMapApiProvider.configure({
                    key: 'AIzaSyB0IYwLyuCSXFG2vdtlRfhp37VeYceA26U',
                    v: '3',
                    libraries: 'weather, geometry, visualization'
                });
                angular.extend($typeaheadProvider.defaults, {
                    autoSelect: true
                });
            }]);