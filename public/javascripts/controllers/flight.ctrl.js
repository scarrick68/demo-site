(function(){
    'use strict';

    angular
        .module('app')
        .controller('flightCtrl', flightCtrl);

    flightCtrl.$inject = ['$scope', '$timeout', '$window', 'flightSrvc', 'airportSrvc'];

    function flightCtrl($scope, $timeout, $window, flightSrvc, airportSrvc){
        // form fields and defaults
        $scope.origin = null;
        $scope.destination = null;
        $scope.embarkDate = new Date();
        $scope.returnDate = new Date();
        $scope.adults = 1;
        $scope.children = 0;
        $scope.nonStop = true;

        $scope.roundTrip = true;
        $scope.oneWay = false;

        $scope.minDate = minDate();
        $scope.airports = [];
        $scope.error = null;
        $scope.flights = [];
        $scope.gettingFlights = null;

        activate();

        function activate() {
            airportSrvc.getAirports().then(
                function (response) {
                    $scope.airports = response.data;
                },
                function () {
                    setErrorMsg('could not retrieve airport data');
                }
            );
        }

        // set default origin and dest if available form airports page. have to work on this feature.
        //if($window.sessionStorage.getItem('origin')){
        //    $scope.origin = $window.sessionStorage.getItem('origin');
        //    $scope.origin = JSON.parse($scope.origin);
        //}
        //if($window.sessionStorage.getItem('destination')){
        //    $scope.destination = $window.sessionStorage.getItem('destination');
        //    $scope.destination = JSON.parse($scope.destination);
        //}

        $scope.getFlights = getFlights;
        $scope.checked = checked;

        // either nonstop or all flights with layovers. making decisions to simplify UI because
        // there are MANY configurable fields with airfare and this isn't KAYAK.
        function getFlights(origin, destination, embarkDate, returnDate, adults, children){
            $scope.gettingFlights = true;
            if($scope.nonStop){
                var maxLayovers = 0;
            }
            else{
                var maxLayovers = 10;
            }
            var tripDetails = {
                origin: origin.iata,
                destination: destination.iata,
                embarkDate: embarkDate,
                returnDate: returnDate,
                adults: adults,
                children: children,
                maxLayovers: maxLayovers,
                roundtrip: $scope.roundTrip
            };

            flightSrvc.getFlights(tripDetails).then(
                function (response) {
                    console.log(response);
                    $scope.flights = response.flights.trips.tripOption;
                    $scope.gettingFlights = false;
                    setErrorMsg();
                },
                function (response) {
                    $scope.gettingFlights = false;
                    // show this error and remove after 10 seconds
                    if(response.code === 403){
                        setErrorMsg('Google Api daily quota has been reached. Please try again tomorrow for flight distance and pricing info.');
                        $timeout(function(){
                            setErrorMsg();
                        }, 10000);
                    }
                    else{
                        setErrorMsg(response.error);
                    }
                }
            );
        }

        // toggle between roundtrip and one way
        function checked(trip){
            if(trip){
                $scope.roundTrip = true;
                $scope.oneWay = false;
            }
            if(!trip){
                $scope.roundTrip = false;
                $scope.oneWay = true;
            }
        }

        // get today's date in proper format for min. valid date
        function minDate(){
            var date = new Date();
            date = date.toISOString().split('T')[0];
            return date;
        }

        // clear error message and set new one
        // calling setErrorMsg with no params will clear error message.
        function setErrorMsg(msg) {
            $scope.error = msg || null;
        }

    }
})();