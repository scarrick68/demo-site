(function(){
    'use strict';

    angular
        .module('app')
        .controller('navCtrl', navCtrl);

    navCtrl.$inject = ['$scope', '$location'];

    function navCtrl($scope, $location){
        $scope.homeClass = null;
        $scope.airportClass = null;
        $scope.flightClass = null;
        $scope.aboutClass = null;
        $scope.projectsClass = null;
        $scope.contactClass = null;

        activate();

        function activate(){
            setActiveItem();
        }

        // call setActiveItem whenever url changes
        $scope.$on('$locationChangeSuccess', function(){
            setActiveItem();
        });

        // set active menu item
        function setActiveItem(){
            var path = $location.path();
            switch (path){
                case '/':
                    unsetMenu();
                    $scope.homeClass = 'selected';
                    break;
                case '/airports':
                    unsetMenu();
                    $scope.airportsClass = 'selected';
                    break;
                case '/flight-info':
                    unsetMenu();
                    $scope.flightClass = 'selected';
                    break;
                case '/about':
                    unsetMenu();
                    $scope.aboutClass = 'selected';
                    break;
                case '/projects':
                    unsetMenu();
                    $scope.projectsClass = 'selected';
                    break;
                case '/contact':
                    unsetMenu();
                    $scope.contactClass = 'selected';
                    break;
                default:
                    unsetMenu();
            }
        }

        // reset menu items to inactive before setting active item
        function unsetMenu(){
            $scope.homeClass = null;
            $scope.airportsClass = null;
            $scope.flightClass = null;
            $scope.aboutClass = null;
            $scope.projectsClass = null;
            $scope.contactClass = null;
        }
    }
})();