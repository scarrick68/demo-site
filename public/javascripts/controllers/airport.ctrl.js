(function(){
    'use strict';

    angular
        .module('app')
        .controller('mapCtrl', mapCtrl);

    mapCtrl.$inject = ['$scope', '$window', '$location', 'airportSrvc', 'distanceSrvc'];

    function mapCtrl($scope, $window, $location, airportSrvc, distanceSrvc){
        // distance variables
        $scope.nauticalMiles = 0;
        $scope.miles = 0;
        $scope.kilometers = 0;

        // airports
        $scope.origin = null;
        $scope.destination = null;

        // map and polyline vars. default to Kansas as center.
        $scope.map = {center: {latitude: 38.5509, longitude: -97.7349}, zoom: 3};
        $scope.path = [];
        $scope.stroke = {
            color: '#6060FB',
            weight: 2
        };
        $scope.visible = true;
        $scope.haveCoordinates = null;

        // UI control, errors, airport data
        $scope.error = null;
        $scope.airports = [];

        // calculate distance, draw map, get min flight prices
        $scope.airportDistance = airportDistance;
        $scope.flightInfo = flightInfo;

        activate();

        function activate() {
            airportSrvc.getAirports().then(
                function (response) {
                    $scope.airports = response.data;
                },
                function () {
                    setErrorMsg('could not retrieve airport data');
                }
            );
        }

        // calculate distance, draw map, get min flight prices
        function airportDistance(origin, destination) {
            distances(origin, destination);
            if (isNaN($scope.nauticalMiles)) {
                $scope.origin = null;
                $scope.destination = null;
                $scope.haveCoordinates = null;
                setErrorMsg('Select An Airport From Each Dropdown');
                distances();
                return;
            }
            $scope.origin = origin;
            $scope.destination = destination;
            setErrorMsg();
            drawMap(origin, destination);
        }

        // store origin and destination and redirect to flight info page
        function flightInfo(){
            $scope.origin = JSON.stringify($scope.origin);
            $scope.destination = JSON.stringify($scope.destination);
            $window.sessionStorage.setItem('origin', $scope.origin);
            $window.sessionStorage.setItem('destination', $scope.destination);
            $location.path('/flight-info');
        }

        // draw polyline on map. Change center and zoom based on airports and distance.
        function drawMap(origin, destination) {
            setZoom($scope.nauticalMiles);
            $scope.map.center = distanceSrvc.midPoint(origin, destination);
            $scope.path = [origin, destination];
            $scope.haveCoordinates = true;
        }

        // set $scope distance variables. reset to 0 if no params passed.
        function distances(origin, destination){
            if(arguments.length === 0){
                $scope.kilometers = 0;
                $scope.nauticalMiles = 0;
                $scope.miles = 0;
                return;
            }
            $scope.kilometers = distanceSrvc.haversine(origin, destination, {unit: 'km'}).toFixed(2);
            $scope.miles = distanceSrvc.haversine(origin, destination, {unit: 'mile'}).toFixed(2);
            $scope.nauticalMiles = distanceSrvc.kmToNauticalMiles($scope.kilometers).toFixed(2);
        }

        // get map zoom based on distance between airports.
        function setZoom(dist) {
            var zoom = 1;
            if (dist < 10000) zoom = 1;
            if (dist < 5000) zoom = 3;
            if (dist < 1000) zoom = 4;
            if (dist < 500) zoom = 7;
            if (dist < 100) zoom = 9;
            if (dist < 10) zoom = 12;
            if (dist < 5) zoom = 16;

            $scope.map.zoom = zoom;
            return;
        }

        // clear error message and set new one
        // calling setErrorMsg with no params will clear error message.
        function setErrorMsg(msg) {
            $scope.error = msg || null;
        }

    }
})();