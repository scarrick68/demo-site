(function(){
    'use strict';

    angular
        .module('app')
        .controller('contactCtrl', contactCtrl);

    contactCtrl.$inject = ['$scope', '$timeout', 'contactSrvc'];

    function contactCtrl($scope, $timeout, contactSrvc){
        $scope.name = null;
        $scope.returnEmail = null;
        $scope.company = null;
        $scope.subject = null;
        $scope.msg = null;
        $scope.sending = false;

        $scope.error = null;
        $scope.success = null;

        $scope.contactMe = contactMe;

        function contactMe(name, returnEmail, company, subject, msg){
            $scope.sending = true;
            var message = {
                name: name,
                returnEmail: returnEmail,
                company: company,
                subject: subject,
                msg: msg
            };
            contactSrvc.sendEmail(message).then(
                function(){
                    setMsg('Email has been sent.', 1);
                    clearForm();
                    $scope.sending = false;
                },
                function(){
                    setMsg('Could send email. Please try again.');
                    $scope.sending = false;
                }
            );
        }

        // type refers to success or error. opposite type is cleared.
        function setMsg(msg, type){
            if(type){
                $scope.error = null;
                $scope.success = msg;
                $timeout(function(){
                    $scope.success = null;
                }, 5000);
            }
            else{
                $scope.success = null;
                $scope.error = msg;
            }
        }

        function clearForm(){
            $scope.name = null;
            $scope.returnEmail = null;
            $scope.company = null;
            $scope.subject = null;
            $scope.msg = null;
        }

    }
})();
