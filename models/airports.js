'use strict';

var mongoose = require('mongoose');
var db = require('./index');

var trimmedString = {type: String, trim: true};

var AirportSchema = new mongoose.Schema({
	id: Number,
	ident: trimmedString,
	type: trimmedString,
	name: trimmedString,
	latitude: Number, 			// latitude in degrees
	longitude: Number, 			// longitude in degrees
	elevation: Number, 			// elevation in feet
	continent: trimmedString,
	iso_country: trimmedString,
	iso_region: trimmedString,
	city: trimmedString,
	scheduled: trimmedString,			// scheduled service 'yes' / 'no'
	gps_code: trimmedString,
	iata: trimmedString,				// iata code
	local_code: trimmedString,
	home_link: trimmedString,
	wikipedia_link: trimmedString,
	keywords: trimmedString
})

AirportSchema.index({name: 1});
AirportSchema.index({latitude: 1});
AirportSchema.index({longitude: 1});
AirportSchema.index({continent: 1});
AirportSchema.index({iso_country: 1});
AirportSchema.index({iso_region: 1});
AirportSchema.index({city: 1});
AirportSchema.index({iata: 1});

module.exports = AirportSchema;