var Mongoose = require('mongoose')

var opts = {
	server: {
		auto_reconnect: true,
		socketOptions: { keepAlive: 1, connectTimeoutMS: 45000}
	}
};

var db = Mongoose.connection;

db.on('connected', function() {
	console.log('Database connection initiated!');
});

db.on('reconnected', function () {
	console.log('MongoDB reconnected!');
});

db.on('error', function(e) {
	console.error('Database connection error: ', e);
	Mongoose.disconnect();
});

db.on('disconnected', function() {
	console.warn('Database disconnected, attempt reconnect...');
	Mongoose.connect('mongodb://localhost:27017/travel', opts);
});

Mongoose.connect('mongodb://localhost:27017/travel', opts);

exports.Airport = Mongoose.model('Airport', require('./airports'));