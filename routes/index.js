'use strict';

exports.index = function(req, res){
  return res.render('layout');
};

exports.partials = function (req, res) {
  var name = req.params.name;
  return res.render('partials/' + name);
};