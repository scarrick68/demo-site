'use strict';

var express = require('express');
var db = require('../models');
var router = express.Router();
var config = require('../config/config');
var request = require('request');
var nodemailer = require('nodemailer');

var airportList;
var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: config.siteEmail,
        pass: config.emailPass
    }
});

router.get('/get-airport-list', function(req, res){
    if(airportList === undefined){
        db.Airport.find({}, {name: 1, latitude: 1, longitude: 1, city: 1, iata: 1}, function(err, airports){
            if(err){
                toStdErr(err);
                return res.json({error: err})
            }
            airportList = airports;
            return res.json({data: airports})
        });
    }
    else{
        return res.json({data: airportList})
    }

});

// get min price for one way flights between given airports.
// can be easily changed to take params like date and number of people
router.post('/get-flight-info', function (req, res) {
    var tripDetails = req.body.tripDetails;
    flights(tripDetails, function(err, flightSolutions){
        if(err){
            toStdErr(err);
            return res.json({error: 'error getting flight solutions', code: err.code});
        }
        return res.json({flights: flightSolutions})
    })
});

// msg {name, subject, company, msg}
// send an email to me
router.post('/contact-me', function(req, res){
   var msg = req.body;
   var mailOptions = {
       from: msg.returnEmail,
       to: config.myEmail,
       subject: msg.subject,
       text: msg.name+' has sent an email:\n'+msg.msg+'\nrespond at: '+msg.returnEmail
   };
    transporter.sendMail(mailOptions, function(err){
        if(err){
            toStdErr(err);
            return res.json({error: err})
        }
        return res.json({status: 'success'})
    })
});

// get all one way flight solutions for given params
function flights(tripDetails, cb){
    var tripReq = tripObj(tripDetails);

    var options = {
        method: 'POST',
        uri: 'https://www.googleapis.com/qpxExpress/v1/trips/search?key='+config.flightApiKey,
        body: tripReq,
        json: true
    };

    request(options, function (err, response, body) {
        if(err) {
            toStdErr(err);
            return cb(err)
        }
        else if(!err && response.statusCode === 200) {
            return cb(null, body)
        }
        toStdErr(body.error);
        return cb(body.error)
    });
}

// make trip object with the given params. Can get big if I take more params so I'm separating it.
function tripObj(tripDetails){
    if(tripDetails.roundtrip){
        var body = {
            'request': {
                'passengers': {
                    'adultCount' : tripDetails.adults,
                    'childCount' : tripDetails.children
                },
                'slice': [
                    {
                        'origin' : tripDetails.origin,
                        'destination' : tripDetails.destination,
                        'date' : formatDate(tripDetails.embarkDate),
                        'maxStops': tripDetails.maxLayovers || 10
                    },
                    {
                        'origin' : tripDetails.destination,
                        'destination' : tripDetails.origin,
                        'date' : formatDate(tripDetails.returnDate),
                        'maxStops': tripDetails.maxLayovers || 10
                    }
                ]
            }
        }
    }
    else{
        var body = {
            'request': {
                'passengers': {
                    'adultCount' : tripDetails.adults,
                    'childCount' : tripDetails.children
                },
                'slice': [
                    {
                        'origin' : tripDetails.origin,
                        'destination' : tripDetails.destination,
                        'date' : formatDate(tripDetails.embarkDate),
                        'maxStops': tripDetails.maxLayovers || 10
                    }
                ]
            }
        }
    }

    return body;
}

// get a date in the format yyyy-mm-dd a given offset from today.
function formatDate(date){
    date = date.split('T')[0];
    return date
}

function toStdErr(err){
    console.error(err);
}

module.exports = router;