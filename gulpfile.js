/* file: gulpfile.js */
var gulp       = require('gulp'),
	jshint     = require('gulp-jshint'),
	sourcemaps = require('gulp-sourcemaps'),
	concat 	   = require('gulp-concat'),
	uglify	   = require('gulp-uglify');

// create a default task and just log a message
gulp.task('default', ['watch', 'build-css', 'build-js']);

gulp.task('watch', function() {
	gulp.watch('public/javascripts/**/*.js', ['jshint']);
	gulp.watch('public/javascripts/**/*.js', ['build-js']);
	gulp.watch('public/css/**/*.css', ['build-css']);
});

gulp.task('build-css', function() {
	return gulp.src('public/css/**/*.css')
		.pipe(sourcemaps.init())  // Process the original sources
		.pipe(concat('app.css'))
		.pipe(sourcemaps.write()) // Add the map to modified source.
		.pipe(gulp.dest('public/dist/'));
});

gulp.task('build-js', function() {
	return gulp.src(['public/javascripts/**/*.js', '!public/javascripts/bower_components/**'])
		.pipe(sourcemaps.init())
		.pipe(concat('full-app.js'))
		.pipe(uglify())
		.pipe(sourcemaps.write())
		.pipe(gulp.dest('public/dist/'));
});

// js linting task
gulp.task('jshint', function() {
	return gulp.src(['public/javascripts/**/*.js', '!public/javascripts/bower_components/**'])
		.pipe(jshint())
		.pipe(jshint.reporter('jshint-stylish'));
});

// only uglify if gulp is ran with '--type production'
